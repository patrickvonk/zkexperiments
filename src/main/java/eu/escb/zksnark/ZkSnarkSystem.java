package eu.escb.zksnark;


import algebra.curves.barreto_naehrig.bn254a.BN254aG1;
import algebra.curves.barreto_naehrig.bn254a.BN254aG2;
import algebra.curves.barreto_naehrig.bn254a.BN254aGT;
import algebra.curves.barreto_naehrig.bn254a.BN254aPairing;
import algebra.curves.barreto_naehrig.bn254a.BN254aFields.BN254aFr;
import algebra.curves.barreto_naehrig.bn254a.bn254a_parameters.BN254aG1Parameters;
import algebra.curves.barreto_naehrig.bn254a.bn254a_parameters.BN254aG2Parameters;
import configuration.Configuration;
import profiler.generation.R1CSConstruction;
import relations.objects.Assignment;
import relations.r1cs.R1CSRelation;
import scala.Tuple3;
import zk_proof_systems.zkSNARK.SerialProver;
import zk_proof_systems.zkSNARK.SerialSetup;
import zk_proof_systems.zkSNARK.Verifier;
import zk_proof_systems.zkSNARK.objects.CRS;
import zk_proof_systems.zkSNARK.objects.Proof;

public class ZkSnarkSystem {
	private Configuration config;
	final int numInputs = 1023;
    final int numConstraints = 1024;
    
    private BN254aFr fieldFactory;
    private BN254aG1 g1Factory;
    private BN254aG2 g2Factory;
    private BN254aPairing pairing;
    
    private R1CSRelation<BN254aFr> r1cs;
    private Assignment<BN254aFr> primary;
    private Assignment<BN254aFr> auxiliary;
    
    private CRS<BN254aFr, BN254aG1, BN254aG2, BN254aGT> CRS;
    private Proof<BN254aG1, BN254aG2> proof;
    
	public void setUp() {
        config = new Configuration();
        config.setRuntimeFlag(false);
        config.setDebugFlag(true);
    }
	
	public void createParameters() {
		fieldFactory = new BN254aFr(15);
        g1Factory = BN254aG1Parameters.ONE;
        g2Factory = BN254aG2Parameters.ONE;
        pairing = new BN254aPairing();
	}
	
	public void initEngine() {
		final Tuple3<R1CSRelation<BN254aFr>, Assignment<BN254aFr>, Assignment<BN254aFr>> construction = R1CSConstruction
                .serialConstruct(numConstraints, numInputs, fieldFactory, config);
        r1cs = construction._1();
        primary = construction._2();
        auxiliary = construction._3();
	}
	
	public void generateKeys( ) {
		CRS = SerialSetup
              .generate(r1cs, fieldFactory, g1Factory, g2Factory, pairing, config);
	}
	
	public void createProof( ) {
		proof = SerialProver.prove(CRS.provingKey(), primary, auxiliary, fieldFactory, config);
	}
	
	public void verifyProof() {
        final boolean isValid = Verifier
                .verify(CRS.verificationKey(), primary, proof, pairing, config);

        System.out.println(isValid);
	}
}
