package eu.escb.zksnark;



public class ZkSnarkRunner {
	
	
	public static void main(String[] args) {
		System.out.println("Start zkSnark prove and verify");
		
		ZkSnarkSystem zkpSystem = new ZkSnarkSystem();
		
		//setup configuration
		zkpSystem.setUp();
		
		zkpSystem.createParameters();
		zkpSystem.initEngine();
		zkpSystem.generateKeys();
		zkpSystem.createProof();
		zkpSystem.verifyProof();
		
	}
	
	

}
